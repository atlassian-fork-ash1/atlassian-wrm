var _ = require('./support/underscore');
var parseAmdDependencies = require('detective-amd');
var fs = require('fs');
var fsPath = require('path');
var support = require('./support');

// Removes extension.
function baseName (nameOrPath) {
    return nameOrPath.replace(/\.[^\.]+$/i, '')
}

// If path has extension.
function hasExtension(nameOrPath) {
    return /\.[^\.]+$/i.test(nameOrPath);
}

// If it's a relative module path or name.
function isRelativeModulePath(nameOrPath) {
    return /^\./.test(nameOrPath);
}

// A synchronous callback
function syncCallback(err, response) {
    if (err) {
        throw err;
    } else {
        return response;
    }
}

// JS module parser.
exports.js = {
    isModulePath: function isJsModulePath (path) {
        return /\.js$/.test(path) && !/\.min\.js$/.test(path)
    },

    _parse: function parseJsModule (modulesDir, modulePath, cb) {
        return function _parse (src) {
            if (_(src).isEmpty()) {
                // Skipping empty JS files.
                return cb(null, false);
            }
            try {
                var dependencies = parseAmdDependencies(src);
            } catch (err) {
                return cb(new Error("can't parse \"" + fsPath.join(modulesDir, modulePath) + "\" module!\nCaused by:\n" + (err.stack || err)));
            }

            // Normalizing dependencies.
            var currentDirPath = fsPath.dirname(modulePath);
            var absoluteModulesDir = fsPath.join(absoluteResourcesBasePath, modulesDir);
            var normalizedDependencies = {};
            for (var i = 0; i < dependencies.length; i++) {
                var dependency = dependencies[i];

                // Parsing loader.
                var parts = dependency.split('!');
                var loader, name;
                if (parts.length == 1) {
                    loader = null;
                    name = parts[0];
                } else if (parts.length == 2) {
                    loader = parts[0];
                    name = parts[1];
                } else {
                    return cb(new Error("invalid dependency name \"" + dependency + "\""));
                }

                // Normalizing path.
                var resolvedName;
                if ((loader != "wr") && isRelativeModulePath(name)) {
                    try {
                        resolvedName = support.normalisePath(absoluteModulesDir, currentDirPath, name);
                    } catch (err) {
                        return cb(err)
                    }
                } else {
                    resolvedName = name;
                }

                var parsedDependency = {
                    resolvedName: resolvedName
                };
                if (loader) {
                    parsedDependency.loader = loader;
                }
                normalizedDependencies[dependency] = parsedDependency;
            }

            // Creating module.
            var relativeFilePath = fsPath.join(modulesDir, modulePath);
            var module = {
                type: 'js',
                srcType: 'js',
                name: modulePath,
                baseName: baseName(modulePath),
                filePath: relativeFilePath
            };
            if (_(normalizedDependencies).size() > 0) {
                module.dependencies = normalizedDependencies
            }
            return cb(null, module);
        };
    },

    // Given path to JS module analyzes it and extracts its dependencies.
    parse: function (absoluteResourcesBasePath, modulesDir, modulePath, cb) {
        fs.readFile(fsPath.join(absoluteResourcesBasePath, modulesDir, modulePath), 'utf8', this.fork(cb, this._parse(modulesDir, modulePath, cb)));
    },

    parseSync: function (absoluteResourcesBasePath, modulesDir, modulePath) {
        var src = fs.readFileSync(fsPath.join(absoluteResourcesBasePath, modulesDir, modulePath), 'utf8');
        return this._parse(modulesDir, modulePath, syncCallback)(src);
    }
};

// CSS module parser, it also parses LESS.
exports.css = {
    isModulePath: function isJsModulePath (path) {
        return /\.(css)$/.test(path) && !/\.min\.(css)$/.test(path)
    },

    // Given path to CSS or LESS file parses it and creates module.
    parse: function parseCssModule (absoluteResourcesBasePath, modulesDir, modulePath, cb) {
        // Creating module.
        var relativeFilePath = fsPath.join(modulesDir, modulePath);
        var module = {
            type: 'css',
            srcType: 'css',
            name: modulePath,
            baseName: baseName(modulePath),
            filePath: relativeFilePath
        };

        return cb(null, module);
    },

    parseSync: function (absoluteResourcesBasePath, modulesDir, modulePath) {
        return this.parse(absoluteResourcesBasePath, modulesDir, modulePath, syncCallback)
    }
};

// LESS module parser.
exports.less = {
    isModulePath: function isJsModulePath (path) {
        return /\.(less)$/.test(path) && !/\.min\.(less)$/.test(path)
    },

    // Given path to CSS or LESS file parses it and creates module.
    parse: function parseLessModule (absoluteResourcesBasePath, modulesDir, modulePath, cb) {
        // Creating module.
        var relativeFilePath = fsPath.join(modulesDir, modulePath);
        var module = {
            type: 'css',
            srcType: 'less',
            name: modulePath,
            baseName: baseName(modulePath),
            filePath: relativeFilePath
        };
        return cb(null, module);
    },

    parseSync: function (absoluteResourcesBasePath, modulesDir, modulePath) {
        return this.parse(absoluteResourcesBasePath, modulesDir, modulePath, syncCallback)
    }
};

// SOY module parser.
exports.soy = {
    isModulePath: function isJsModulePath (path) {
        return /\.(soy)$/.test(path)
    },

    // Given path to SOY file parses it and creates module.
    _parse: function parseSoyModule (modulesDir, modulePath, cb) {
        return function _parse (src) {
            // Parsing namespace.
            var match = /\{\s*namespace\s*([^\s/]+)\s*\}/.exec(src);
            if (!match) {
                return cb(new Error("no namespace for \"" + fsPath.join(modulesDir, modulePath) + "\""));
            }
            var soyNamespace = match[1];

            // Parsing templates.
            var soyTemplates = [];
            var templateRe = /\{\s*template\s+([^\s/\}]+)/g;
            match = templateRe.exec(src);
            while(match) {
                var template = match[1];
                // Converting relative soy template names to absolute.
                soyTemplates.push(/^\./.test(template) ? soyNamespace + template : template);
                match = templateRe.exec(src);
            }

            // Parsing calls to templates.
            var soyDependencies = [];
            var callRe = /\{\s*call\s+([^\s/\}]+)/g;
            match = callRe.exec(src);
            while(match) {
                var call = match[1];
                // Converting relative soy template names to absolute.
                soyDependencies.push(/^\./.test(call) ? soyNamespace + call : call);
                match = callRe.exec(src);
            }

            // Creating module.
            var relativeFilePath = fsPath.join(modulesDir, modulePath);
            var module = {
                type: 'js',
                srcType: 'soy',
                soyNamespace: soyNamespace,
                name: modulePath,
                baseName: baseName(modulePath),
                filePath: relativeFilePath
            };
            if (soyTemplates.length > 0) {
              module.soyTemplates = soyTemplates
            }
            if (soyNamespace.length > 0) {
              module.soyDependencies = soyDependencies
            }
            return cb(null, module);
        };
    },

    parse: function (absoluteResourcesBasePath, modulesDir, modulePath, cb) {
        fs.readFile(fsPath.join(absoluteResourcesBasePath, modulesDir, modulePath), 'utf8', this.fork(cb, this._parse(modulesDir, modulePath, cb)));
    },

    parseSync: function (absoluteResourcesBasePath, modulesDir, modulePath) {
        var src = fs.readFileSync(fsPath.join(absoluteResourcesBasePath, modulesDir, modulePath), 'utf8');
        return this._parse(modulesDir, modulePath, syncCallback)(src);
    }
};