var _ = require('underscore');
module.exports = _;

function ensureCalledOnce (fn) {
    var called = false;
    return function ensureCalledOnceWrapper () {
        if (called) throw new Error("can be called only once!");
        called = true;
        return fn.apply(this, arguments);
    }
}

_.mixin({
    // Helps to deal with async stuff.
    fork: function fork (onError, onSuccess) {
        if (!onError) throw new Error("error callback not defined!");
        onError = ensureCalledOnce(onError);

        if (!onSuccess) throw new Error("success callback not defined!");
        onSuccess = ensureCalledOnce(onSuccess);

        return function () {
            var err = arguments[0];
            var argsWithoutError = Array.prototype.slice.call(arguments, 1);
            !!err ? onError(err) : onSuccess.apply(null, argsWithoutError);
        }
    }
});